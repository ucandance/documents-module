<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;
use Finnito\DocumentsModule\DocumentCategory\DocumentCategoryModel;
use Finnito\DocumentsModule\MinuteCategory\MinuteCategoryModel;
use Finnito\DocumentsModule\CommitteeDocumentCategory\CommitteeDocumentCategoryModel;
use Finnito\DocumentsModule\GeneralMinuteCategory\GeneralMinuteCategoryModel;
use Finnito\DocumentsModule\PolicyCategory\PolicyCategoryModel;

class FinnitoModuleDocumentsCreateDocumentsFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        /* Shared Fields */
        "name" => "anomaly.field_type.text",
        "slug" => [
            "type" => "anomaly.field_type.slug",
            "config" => [
                "slugify" => "name",
                "type" => "-",
            ],
        ],
        "archive" => [
            "type" => "anomaly.field_type.boolean",
            "config" => [
                "on_text" => "Yes",
                "off_text" => "No",
            ],
        ],
        "year" => [
            "type" => "anomaly.field_type.integer",
            "config" => [
                "separator" => null,
            ],
        ],
        "date" => [
            "type" => "anomaly.field_type.datetime",
            "config" => [
                "mode" => "date",
                "date_format" => "Y-m-d",
            ],
        ],
        "description" => [
            "type" => "anomaly.field_type.textarea",
            "config" => [
                "rows" => 3,
                "max" => 256,
                "show_counter" => true,
            ],
        ],

        /* Documents Fields */
        "documents" => [
            "type" => "anomaly.field_type.files",
            "config" => [
                "folders" => [
                    "documents",
                ],
            ],
        ],
        "document_category" => [
            "type" => "anomaly.field_type.relationship",
            "config" => [
                "related" => DocumentCategoryModel::class,
                "mode" => "search",
            ],
        ],

        /* Minutes Fields */
        "minutes" => [
            "type" => "anomaly.field_type.files",
            "config" => [
                "folders" => [
                    "minutes",
                ],
            ],
        ],
        "minute_category" => [
            "type" => "anomaly.field_type.relationship",
            "config" => [
                "related" => MinuteCategoryModel::class,
                "mode" => "search",
            ],
        ],

        /* Committee Documents Fields */
        "committee_documents" => [
            "type" => "anomaly.field_type.files",
            "config" => [
                "folders" => [
                    "committee_documents",
                ],
            ],
        ],
        "committee_document_category" => [
            "type" => "anomaly.field_type.relationship",
            "config" => [
                "related" => CommitteeDocumentCategoryModel::class,
                "mode" => "search",
            ],
        ],

        /* General Minutes Fields */
        "general_minutes" => [
            "type" => "anomaly.field_type.files",
            "config" => [
                "folders" => [
                    "general_minutes",
                ],
            ],
        ],
        "general_minute_category" => [
            "type" => "anomaly.field_type.relationship",
            "config" => [
                "related" => GeneralMinuteCategoryModel::class,
                "mode" => "search",
            ],
        ],

        /* Policy Fields */
        "policy" => [
            "type" => "anomaly.field_type.files",
            "config" => [
                "folders" => [
                    "policies",
                ],
            ],
        ],
        "policy_category" => [
            "type" => "anomaly.field_type.relationship",
            "config" => [
                "related" => PolicyCategoryModel::class,
                "mode" => "search",
            ],
        ],
    ];

}
