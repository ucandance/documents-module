<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FinnitoModuleDocumentsCreateMinutesStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'minutes',
         'title_column' => 'name',
         'translatable' => false,
         'versionable' => false,
         'trashable' => false,
         'searchable' => true,
         'sortable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name' => [
            'required' => true,
        ],
        'slug' => [
            'unique' => true,
            'required' => true,
        ],
        "date",
        "minutes",
        "minute_category",
        "description",
    ];
}
