<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FinnitoModuleDocumentsCreateGeneralMinutesStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'general_minutes',
         'title_column' => 'name',
         'translatable' => false,
         'versionable' => false,
         'trashable' => false,
         'searchable' => true,
         'sortable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name' => [
            'required' => true,
        ],
        'slug' => [
            'unique' => true,
            'required' => true,
        ],
        "general_minutes",
        "general_minute_category",
        "date",
        "description",
    ];

}
