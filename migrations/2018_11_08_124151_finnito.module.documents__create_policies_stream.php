<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FinnitoModuleDocumentsCreatePoliciesStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'policies',
         'title_column' => 'name',
         'translatable' => false,
         'versionable' => false,
         'trashable' => false,
         'searchable' => true,
         'sortable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name' => [
            'required' => true,
        ],
        'slug' => [
            'unique' => true,
            'required' => true,
        ],
        "policy",
        "policy_category",
        "date",
        "description",
    ];

}
