<?php

return [

    'documents' => [
        'read',
        'write',
        'delete',
    ],
    'document_categories' => [
        'read',
        'write',
        'delete',
    ],
    'minutes' => [
        'read',
        'write',
        'delete',
    ],
    'minute_categories' => [
        'read',
        'write',
        'delete',
    ],
    'committee_documents' => [
        'read',
        'write',
        'delete',
    ],
    'committee_document_categories' => [
        'read',
        'write',
        'delete',
    ],
    'general_minutes' => [
        'read',
        'write',
        'delete',
    ],
    'general_minute_categories' => [
        'read',
        'write',
        'delete',
    ],
    'policies' => [
        'read',
        'write',
        'delete',
    ],
    'policy_categories' => [
        'read',
        'write',
        'delete',
    ],
];
