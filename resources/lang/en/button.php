<?php

return [
    'new_document' => 'New Document',
    'new_document_category' => 'New Document Category',
    'new_minute' => 'New Minutes',
    'new_minute_category' => 'New Minute Category',
    'new_committee_document' => 'New Committee Document',
    'new_committee_document_category' => 'New Committee Document Category',
    'new_general_minute' => 'New General Minutes',
    'new_general_minute_category' => 'New General Minute Category',
    'new_policy' => 'New Policy',
    'new_policy_category' => 'New Policy Category',
];
