<?php

return [
	/* Shared Fields */
	"name" => [
		"name" => "Name",
		"instructions" => "",
	],
	"slug" => [
		"name" => "Slug",
		"instructions" => "Do not change me. Automatically set.",
	],
	"archive" => [
		"name" => "Archived Status",
		"instructions" => "Archived files do not show up under the default filters to cut down on clutter.",
	],
	"year" => [
		"name" => "Year",
		"instructions" => "The year this document was last updated.",
	],
	"date" => [
		"name" => "Date",
		"instructions" => "The date of the meeting, last update of the document etc.",
	],
	"description" => [
		"name" => "Description",
		"instructions" => "Example: List of AGM topics, deprecated policy notice, useful information about the document.",
	],

	/* Documents Fields */
	"documents" => [
		"name" => "Documents",
		"instructions" => "Upload and/or select the files for this entry. Select multiple files if the document exists in multiple formats e.g. .docx and .pdf.",
	],
	"document_category" => [
		"name" => "Category",
		"instructions" => "The category that this document belongs to.",
	],

	/* Minutes Fields */
	"minutes" => [
		"name" => "Minutes",
		"instructions" => "Upload and/or select the files for this entry. Select multiple files if the document exists in multiple formats e.g. .docx and .pdf.",
	],
	"minute_category" => [
		"name" => "Category",
		"instructions" => "The category that these minutes belongs to.",
	],

	/* Committee Documents Fields */
	"committee_documents" => [
		"name" => "Documents",
		"instructions" => "Upload and/or select the files for this entry. Select multiple files if the document exists in multiple formats e.g. .docx and .pdf.",
	],
	"committee_document_category" => [
		"name" => "Category",
		"instructions" => "The category that these committee documents belong to.",
	],

	/* General Minutes Fields */
	"general_minutes" => [
		"name" => "Minutes",
		"instructions" => "Upload and/or select the files for this entry. Select multiple files if the document exists in multiple formats e.g. .docx and .pdf.",
	],
	"general_minute_category" => [
		"name" => "Category",
		"instructions" => "The category that these general minutes belong to.",
	],

	/* Policy Fields */
	"policy" => [
		"name" => "Policy",
		"instructions" => "Upload and/or select the files for this entry. Select multiple files if the document exists in multiple formats e.g. .docx and .pdf.",
	],
	"policy_category" => [
		"name" => "Policy",
		"instructions" => "The policy that this version belongs to.",
	],
];