<?php

return [
    'documents' => [
        'name'   => 'Documents',
        'option' => [
            'read'   => 'Can read documents?',
            'write'  => 'Can create/edit documents?',
            'delete' => 'Can delete documents?',
        ],
    ],
    'document_categories' => [
        'name'   => 'Document categories',
        'option' => [
            'read'   => 'Can read document categories?',
            'write'  => 'Can create/edit document categories?',
            'delete' => 'Can delete document categories?',
        ],
    ],
    'minutes' => [
        'name'   => 'Minutes',
        'option' => [
            'read'   => 'Can read minutes?',
            'write'  => 'Can create/edit minutes?',
            'delete' => 'Can delete minutes?',
        ],
    ],
    'minute_categories' => [
        'name'   => 'Minute categories',
        'option' => [
            'read'   => 'Can read minute categories?',
            'write'  => 'Can create/edit minute categories?',
            'delete' => 'Can delete minute categories?',
        ],
    ],
    'committee_documents' => [
        'name'   => 'Committee documents',
        'option' => [
            'read'   => 'Can read committee documents?',
            'write'  => 'Can create/edit committee documents?',
            'delete' => 'Can delete committee documents?',
        ],
    ],
    'committee_document_categories' => [
        'name'   => 'Committee document categories',
        'option' => [
            'read'   => 'Can read committee document categories?',
            'write'  => 'Can create/edit committee document categories?',
            'delete' => 'Can delete committee document categories?',
        ],
    ],
    'general_minutes' => [
        'name'   => 'General minutes',
        'option' => [
            'read'   => 'Can read general minutes?',
            'write'  => 'Can create/edit general minutes?',
            'delete' => 'Can delete general minutes?',
        ],
    ],
    'general_minute_categories' => [
        'name'   => 'General minute categories',
        'option' => [
            'read'   => 'Can read general minute categories?',
            'write'  => 'Can create/edit general minute categories?',
            'delete' => 'Can delete general minute categories?',
        ],
    ],
    'policies' => [
        'name'   => 'Policies',
        'option' => [
            'read'   => 'Can read policies?',
            'write'  => 'Can create/edit policies?',
            'delete' => 'Can delete policies?',
        ],
    ],
    'policy_categories' => [
        'name'   => 'Policy categories',
        'option' => [
            'read'   => 'Can read policy categories?',
            'write'  => 'Can create/edit policy categories?',
            'delete' => 'Can delete policy categories?',
        ],
    ],
];
