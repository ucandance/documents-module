<?php

return [
    'documents' => [
        'title' => 'Documents',
    ],
    'document_categories' => [
        'title' => '&rdca; Categories',
    ],
    'minutes' => [
        'title' => 'Minutes',
    ],
    'minute_categories' => [
        'title' => '&rdca; Categories',
    ],
    'committee_documents' => [
        'title' => 'Committee Docs.',
    ],
    'committee_document_categories' => [
        'title' => '&rdca; Categories',
    ],
    'general_minutes' => [
        'title' => 'Gen. Minutes',
    ],
    'general_minute_categories' => [
        'title' => '&rdca; Categories',
    ],
    'policies' => [
        'title' => 'Policies',
    ],
    'policy_categories' => [
        'title' => '&rdca; Categories',
    ],
];
