<?php

return [
    'documents' => [
        'name' => 'Documents',
    ],
    'document_categories' => [
        'name' => 'Document categories',
    ],
    'minutes' => [
        'name' => 'Minutes',
    ],
    'minute_categories' => [
        'name' => 'Minute categories',
    ],
    'committee_documents' => [
        'name' => 'Committee documents',
    ],
    'committee_document_categories' => [
        'name' => 'Committee document categories',
    ],
    'general_minutes' => [
        'name' => 'General minutes',
    ],
    'general_minute_categories' => [
        'name' => 'General minute categories',
    ],
    'policies' => [
        'name' => 'Policies',
    ],
    'policy_categories' => [
        'name' => 'Policy categories',
    ],
];
