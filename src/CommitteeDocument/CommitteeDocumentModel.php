<?php namespace Finnito\DocumentsModule\CommitteeDocument;

use Finnito\DocumentsModule\CommitteeDocument\Contract\CommitteeDocumentInterface;
use Anomaly\Streams\Platform\Model\Documents\DocumentsCommitteeDocumentsEntryModel;

class CommitteeDocumentModel extends DocumentsCommitteeDocumentsEntryModel implements CommitteeDocumentInterface
{
    public function formats()
    {
        $formats = [];
        for ($i = 0; $i < sizeof($this->committee_documents); $i++) {
            $formats[] = "
            <a
                href='{$this->committee_documents[$i]->route("download")}'
                alt='{$this->committee_documents[$i]->name}'>
                .{$this->committee_documents[$i]->extension}
            </a>";
        }
        return implode("", $formats);
    }
}
