<?php namespace Finnito\DocumentsModule\CommitteeDocument;

use Anomaly\Streams\Platform\Entry\EntryPresenter;

class CommitteeDocumentPresenter extends EntryPresenter
{
    public function formats()
    {
        $formats = [];
        for ($i = 0; $i < sizeof($this->committee_documents); $i++) {
            $formats[] = "
            <a
                href='{$this->committee_documents[$i]->route("download")}'
                alt='{$this->committee_documents[$i]->name}'>
                <span class='tag tag-info tag-sm'>.{$this->committee_documents[$i]->extension}</span>
            </a>";
        }
        return implode("", $formats);
    }
}
