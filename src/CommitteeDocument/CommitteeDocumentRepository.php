<?php namespace Finnito\DocumentsModule\CommitteeDocument;

use Finnito\DocumentsModule\CommitteeDocument\Contract\CommitteeDocumentRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class CommitteeDocumentRepository extends EntryRepository implements CommitteeDocumentRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var CommitteeDocumentModel
     */
    protected $model;

    /**
     * Create a new CommitteeDocumentRepository instance.
     *
     * @param CommitteeDocumentModel $model
     */
    public function __construct(CommitteeDocumentModel $model)
    {
        $this->model = $model;
    }
}
