<?php namespace Finnito\DocumentsModule\CommitteeDocument\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface CommitteeDocumentRepositoryInterface extends EntryRepositoryInterface
{

}
