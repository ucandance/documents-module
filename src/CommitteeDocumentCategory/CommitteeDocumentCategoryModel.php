<?php namespace Finnito\DocumentsModule\CommitteeDocumentCategory;

use Finnito\DocumentsModule\CommitteeDocumentCategory\Contract\CommitteeDocumentCategoryInterface;
use Anomaly\Streams\Platform\Model\Documents\DocumentsCommitteeDocumentCategoriesEntryModel;
use Finnito\DocumentsModule\CommitteeDocument\CommitteeDocumentModel;

class CommitteeDocumentCategoryModel extends DocumentsCommitteeDocumentCategoriesEntryModel implements CommitteeDocumentCategoryInterface
{
    public function getEntries()
    {
        return $this
            ->hasMany(CommitteeDocumentModel::class, "committee_document_category_id")
            ->get();
    }
}
