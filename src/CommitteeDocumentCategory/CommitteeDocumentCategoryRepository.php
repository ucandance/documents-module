<?php namespace Finnito\DocumentsModule\CommitteeDocumentCategory;

use Finnito\DocumentsModule\CommitteeDocumentCategory\Contract\CommitteeDocumentCategoryRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class CommitteeDocumentCategoryRepository extends EntryRepository implements CommitteeDocumentCategoryRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var CommitteeDocumentCategoryModel
     */
    protected $model;

    /**
     * Create a new CommitteeDocumentCategoryRepository instance.
     *
     * @param CommitteeDocumentCategoryModel $model
     */
    public function __construct(CommitteeDocumentCategoryModel $model)
    {
        $this->model = $model;
    }
}
