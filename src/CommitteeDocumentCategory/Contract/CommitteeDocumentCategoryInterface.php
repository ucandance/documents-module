<?php namespace Finnito\DocumentsModule\CommitteeDocumentCategory\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

interface CommitteeDocumentCategoryInterface extends EntryInterface
{

}
