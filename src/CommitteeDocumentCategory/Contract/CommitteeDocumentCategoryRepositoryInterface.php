<?php namespace Finnito\DocumentsModule\CommitteeDocumentCategory\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface CommitteeDocumentCategoryRepositoryInterface extends EntryRepositoryInterface
{

}
