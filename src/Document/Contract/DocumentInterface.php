<?php namespace Finnito\DocumentsModule\Document\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

interface DocumentInterface extends EntryInterface
{

}
