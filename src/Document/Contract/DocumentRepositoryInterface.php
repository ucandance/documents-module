<?php namespace Finnito\DocumentsModule\Document\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface DocumentRepositoryInterface extends EntryRepositoryInterface
{

}
