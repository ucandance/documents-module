<?php namespace Finnito\DocumentsModule\Document;

use Finnito\DocumentsModule\Document\Contract\DocumentInterface;
use Anomaly\Streams\Platform\Model\Documents\DocumentsDocumentsEntryModel;

class DocumentModel extends DocumentsDocumentsEntryModel implements DocumentInterface
{
    public function entry()
    {
        if ($this->description) {
            return $this->name . "<br>" . "<p class='sub'>{$this->description}</p>";
        } else {
            return $this->name;
        }
    }
    public function current()
    {
        if ($this->archive) {
            return "Archived";
        }
        else {
            return "Current";
        }
    }

    public function category()
    {
        return $this->document_category;
    }

    public function formats()
    {
        $formats = [];
        for ($i = 0; $i < sizeof($this->documents); $i++) {
            $formats[] = "
            <a
                href='{$this->documents[$i]->route("download")}'
                alt='{$this->documents[$i]->name}'>
                .{$this->documents[$i]->extension}
            </a>";
        }
        return implode("", $formats);
    }

    public function classes($attributes)
    {
        $classes = [];

        // Year
        array_push($classes, "y-{$this->year}");

        // Archived
        if ($this->archive) {
            array_push($classes, "archived");
        } else {
            array_push($classes, "current");
        }

        // Category
        array_push($classes, $this->document_category->slug);

        return implode(" ", $classes);
    }
}
