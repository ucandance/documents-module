<?php namespace Finnito\DocumentsModule\Document;

use Anomaly\Streams\Platform\Entry\EntryPresenter;

class DocumentPresenter extends EntryPresenter
{
    /**
     * Return the status of the event
     * as a string of HTML.
     *
     * @return null|string
     */
    public function statusLabel()
    {
        $size = 'sm';
        $color  = 'default';
        $status = $this->status();
        $pub = "";

        switch ($status) {
            case 'Current':
                $color = 'success';
                break;

            case 'Archived':
                $color = 'warning';
                break;
        }

        return '<span class="tag tag-' . $size . ' tag-' . $color . '">' .  $status . '</span>';
    }

    public function formats()
    {
        $formats = [];
        for ($i = 0; $i < sizeof($this->documents); $i++) {
            $formats[] = "
            <a
                href='{$this->documents[$i]->route("download")}'
                alt='{$this->documents[$i]->name}'>
                <span class='tag tag-info tag-sm'>.{$this->documents[$i]->extension}</span>
            </a>";
        }
        return implode("", $formats);
    }

    /**
     * Return the status of the event
     *
     * @return bool
     */
    public function status()
    {
        if ($this->object->archive) {
            return 'Archived';
        } else {
            return 'Current';
        }

        return null;
    }
}
