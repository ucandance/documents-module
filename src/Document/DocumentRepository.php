<?php namespace Finnito\DocumentsModule\Document;

use Finnito\DocumentsModule\Document\Contract\DocumentRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class DocumentRepository extends EntryRepository implements DocumentRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var DocumentModel
     */
    protected $model;

    /**
     * Create a new DocumentRepository instance.
     *
     * @param DocumentModel $model
     */
    public function __construct(DocumentModel $model)
    {
        $this->model = $model;
    }
}
