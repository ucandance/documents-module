<?php namespace Finnito\DocumentsModule\Document\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

class DocumentTableBuilder extends TableBuilder
{

    /**
     * The table views.
     *
     * @var array|string
     */
    protected $views = [];

    /**
     * The table filters.
     *
     * @var array|string
     */
    protected $filters = [];

    /**
     * The table columns.
     *
     * @var array|string
     */
    protected $columns = [
        "status" => [
            "heading" => "Status",
            "value" => "entry.statusLabel()",
        ],
        "name",
        "document_category",
        "year",
        "downloads" => [
            "heading" => "Formats",
            "value" => "entry.formats()",
        ],
    ];

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'edit'
    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete'
    ];

    /**
     * The table options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The table assets.
     *
     * @var array
     */
    protected $assets = [];

}
