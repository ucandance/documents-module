<?php namespace Finnito\DocumentsModule\DocumentCategory\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

interface DocumentCategoryInterface extends EntryInterface
{

}
