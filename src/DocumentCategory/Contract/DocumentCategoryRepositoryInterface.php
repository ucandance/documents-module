<?php namespace Finnito\DocumentsModule\DocumentCategory\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface DocumentCategoryRepositoryInterface extends EntryRepositoryInterface
{

}
