<?php namespace Finnito\DocumentsModule\DocumentCategory;

use Finnito\DocumentsModule\DocumentCategory\Contract\DocumentCategoryInterface;
use Anomaly\Streams\Platform\Model\Documents\DocumentsDocumentCategoriesEntryModel;

use Finnito\DocumentsModule\Document\DocumentModel;

class DocumentCategoryModel extends DocumentsDocumentCategoriesEntryModel implements DocumentCategoryInterface
{
    public function getEntries()
    {
        return $this
            ->hasMany(DocumentModel::class, "document_category_id")
            ->get();
    }
}
