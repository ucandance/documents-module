<?php namespace Finnito\DocumentsModule\DocumentCategory;

use Finnito\DocumentsModule\DocumentCategory\Contract\DocumentCategoryRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class DocumentCategoryRepository extends EntryRepository implements DocumentCategoryRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var DocumentCategoryModel
     */
    protected $model;

    /**
     * Create a new DocumentCategoryRepository instance.
     *
     * @param DocumentCategoryModel $model
     */
    public function __construct(DocumentCategoryModel $model)
    {
        $this->model = $model;
    }
}
