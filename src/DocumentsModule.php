<?php namespace Finnito\DocumentsModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

class DocumentsModule extends Module
{

    /**
     * The navigation display flag.
     *
     * @var bool
     */
    protected $navigation = true;

    /**
     * The addon icon.
     *
     * @var string
     */
    protected $icon = 'fa fa-file';

    /**
     * The module sections.
     *
     * @var array
     */
    protected $sections = [
        'documents' => [
            'buttons' => [
                'new_document',
            ],
        ],
        'document_categories' => [
            'buttons' => [
                'new_document_category',
            ],
        ],
        'minutes' => [
            'buttons' => [
                'new_minute',
            ],
        ],
        'minute_categories' => [
            'buttons' => [
                'new_minute_category',
            ],
        ],
        'committee_documents' => [
            'buttons' => [
                'new_committee_document',
            ],
        ],
        'committee_document_categories' => [
            'buttons' => [
                'new_committee_document_category',
            ],
        ],
        'general_minutes' => [
            'buttons' => [
                'new_general_minute',
            ],
        ],
        'general_minute_categories' => [
            'buttons' => [
                'new_general_minute_category',
            ],
        ],
        'policies' => [
            'buttons' => [
                'new_policy',
            ],
        ],
        'policy_categories' => [
            'buttons' => [
                'new_policy_category',
            ],
        ],
    ];

}
