<?php namespace Finnito\DocumentsModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Finnito\DocumentsModule\PolicyCategory\Contract\PolicyCategoryRepositoryInterface;
use Finnito\DocumentsModule\PolicyCategory\PolicyCategoryRepository;
use Anomaly\Streams\Platform\Model\Documents\DocumentsPolicyCategoriesEntryModel;
use Finnito\DocumentsModule\PolicyCategory\PolicyCategoryModel;
use Finnito\DocumentsModule\Policy\Contract\PolicyRepositoryInterface;
use Finnito\DocumentsModule\Policy\PolicyRepository;
use Anomaly\Streams\Platform\Model\Documents\DocumentsPoliciesEntryModel;
use Finnito\DocumentsModule\Policy\PolicyModel;
use Finnito\DocumentsModule\GeneralMinuteCategory\Contract\GeneralMinuteCategoryRepositoryInterface;
use Finnito\DocumentsModule\GeneralMinuteCategory\GeneralMinuteCategoryRepository;
use Anomaly\Streams\Platform\Model\Documents\DocumentsGeneralMinuteCategoriesEntryModel;
use Finnito\DocumentsModule\GeneralMinuteCategory\GeneralMinuteCategoryModel;
use Finnito\DocumentsModule\GeneralMinute\Contract\GeneralMinuteRepositoryInterface;
use Finnito\DocumentsModule\GeneralMinute\GeneralMinuteRepository;
use Anomaly\Streams\Platform\Model\Documents\DocumentsGeneralMinutesEntryModel;
use Finnito\DocumentsModule\GeneralMinute\GeneralMinuteModel;
use Finnito\DocumentsModule\CommitteeDocumentCategory\Contract\CommitteeDocumentCategoryRepositoryInterface;
use Finnito\DocumentsModule\CommitteeDocumentCategory\CommitteeDocumentCategoryRepository;
use Anomaly\Streams\Platform\Model\Documents\DocumentsCommitteeDocumentCategoriesEntryModel;
use Finnito\DocumentsModule\CommitteeDocumentCategory\CommitteeDocumentCategoryModel;
use Finnito\DocumentsModule\CommitteeDocument\Contract\CommitteeDocumentRepositoryInterface;
use Finnito\DocumentsModule\CommitteeDocument\CommitteeDocumentRepository;
use Anomaly\Streams\Platform\Model\Documents\DocumentsCommitteeDocumentsEntryModel;
use Finnito\DocumentsModule\CommitteeDocument\CommitteeDocumentModel;
use Finnito\DocumentsModule\MinuteCategory\Contract\MinuteCategoryRepositoryInterface;
use Finnito\DocumentsModule\MinuteCategory\MinuteCategoryRepository;
use Anomaly\Streams\Platform\Model\Documents\DocumentsMinuteCategoriesEntryModel;
use Finnito\DocumentsModule\MinuteCategory\MinuteCategoryModel;
use Finnito\DocumentsModule\Minute\Contract\MinuteRepositoryInterface;
use Finnito\DocumentsModule\Minute\MinuteRepository;
use Anomaly\Streams\Platform\Model\Documents\DocumentsMinutesEntryModel;
use Finnito\DocumentsModule\Minute\MinuteModel;
use Finnito\DocumentsModule\DocumentCategory\Contract\DocumentCategoryRepositoryInterface;
use Finnito\DocumentsModule\DocumentCategory\DocumentCategoryRepository;
use Anomaly\Streams\Platform\Model\Documents\DocumentsDocumentCategoriesEntryModel;
use Finnito\DocumentsModule\DocumentCategory\DocumentCategoryModel;
use Finnito\DocumentsModule\Document\Contract\DocumentRepositoryInterface;
use Finnito\DocumentsModule\Document\DocumentRepository;
use Anomaly\Streams\Platform\Model\Documents\DocumentsDocumentsEntryModel;
use Finnito\DocumentsModule\Document\DocumentModel;
use Illuminate\Routing\Router;

class DocumentsModuleServiceProvider extends AddonServiceProvider
{

    /**
     * Additional addon plugins.
     *
     * @type array|null
     */
    protected $plugins = [];

    /**
     * The addon Artisan commands.
     *
     * @type array|null
     */
    protected $commands = [];

    /**
     * The addon's scheduled commands.
     *
     * @type array|null
     */
    protected $schedules = [];

    /**
     * The addon API routes.
     *
     * @type array|null
     */
    protected $api = [];

    /**
     * The addon routes.
     *
     * @type array|null
     */
    protected $routes = [
        'admin/documents/policy_categories'           => 'Finnito\DocumentsModule\Http\Controller\Admin\PolicyCategoriesController@index',
        'admin/documents/policy_categories/create'    => 'Finnito\DocumentsModule\Http\Controller\Admin\PolicyCategoriesController@create',
        'admin/documents/policy_categories/edit/{id}' => 'Finnito\DocumentsModule\Http\Controller\Admin\PolicyCategoriesController@edit',
        'admin/documents/policies'           => 'Finnito\DocumentsModule\Http\Controller\Admin\PoliciesController@index',
        'admin/documents/policies/create'    => 'Finnito\DocumentsModule\Http\Controller\Admin\PoliciesController@create',
        'admin/documents/policies/edit/{id}' => 'Finnito\DocumentsModule\Http\Controller\Admin\PoliciesController@edit',
        'admin/documents/general_minute_categories'           => 'Finnito\DocumentsModule\Http\Controller\Admin\GeneralMinuteCategoriesController@index',
        'admin/documents/general_minute_categories/create'    => 'Finnito\DocumentsModule\Http\Controller\Admin\GeneralMinuteCategoriesController@create',
        'admin/documents/general_minute_categories/edit/{id}' => 'Finnito\DocumentsModule\Http\Controller\Admin\GeneralMinuteCategoriesController@edit',
        'admin/documents/general_minutes'           => 'Finnito\DocumentsModule\Http\Controller\Admin\GeneralMinutesController@index',
        'admin/documents/general_minutes/create'    => 'Finnito\DocumentsModule\Http\Controller\Admin\GeneralMinutesController@create',
        'admin/documents/general_minutes/edit/{id}' => 'Finnito\DocumentsModule\Http\Controller\Admin\GeneralMinutesController@edit',
        'admin/documents/committee_document_categories'           => 'Finnito\DocumentsModule\Http\Controller\Admin\CommitteeDocumentCategoriesController@index',
        'admin/documents/committee_document_categories/create'    => 'Finnito\DocumentsModule\Http\Controller\Admin\CommitteeDocumentCategoriesController@create',
        'admin/documents/committee_document_categories/edit/{id}' => 'Finnito\DocumentsModule\Http\Controller\Admin\CommitteeDocumentCategoriesController@edit',
        'admin/documents/committee_documents'           => 'Finnito\DocumentsModule\Http\Controller\Admin\CommitteeDocumentsController@index',
        'admin/documents/committee_documents/create'    => 'Finnito\DocumentsModule\Http\Controller\Admin\CommitteeDocumentsController@create',
        'admin/documents/committee_documents/edit/{id}' => 'Finnito\DocumentsModule\Http\Controller\Admin\CommitteeDocumentsController@edit',
        'admin/documents/minute_categories'           => 'Finnito\DocumentsModule\Http\Controller\Admin\MinuteCategoriesController@index',
        'admin/documents/minute_categories/create'    => 'Finnito\DocumentsModule\Http\Controller\Admin\MinuteCategoriesController@create',
        'admin/documents/minute_categories/edit/{id}' => 'Finnito\DocumentsModule\Http\Controller\Admin\MinuteCategoriesController@edit',
        'admin/documents/minutes'           => 'Finnito\DocumentsModule\Http\Controller\Admin\MinutesController@index',
        'admin/documents/minutes/create'    => 'Finnito\DocumentsModule\Http\Controller\Admin\MinutesController@create',
        'admin/documents/minutes/edit/{id}' => 'Finnito\DocumentsModule\Http\Controller\Admin\MinutesController@edit',
        'admin/documents/document_categories'           => 'Finnito\DocumentsModule\Http\Controller\Admin\DocumentCategoriesController@index',
        'admin/documents/document_categories/create'    => 'Finnito\DocumentsModule\Http\Controller\Admin\DocumentCategoriesController@create',
        'admin/documents/document_categories/edit/{id}' => 'Finnito\DocumentsModule\Http\Controller\Admin\DocumentCategoriesController@edit',
        'admin/documents'           => 'Finnito\DocumentsModule\Http\Controller\Admin\DocumentsController@index',
        'admin/documents/create'    => 'Finnito\DocumentsModule\Http\Controller\Admin\DocumentsController@create',
        'admin/documents/edit/{id}' => 'Finnito\DocumentsModule\Http\Controller\Admin\DocumentsController@edit',

        // Frontend Routes
        "documents"             =>  "Finnito\DocumentsModule\Http\Controller\DocumentsController@documents",
        "documents/committee"   =>  "Finnito\DocumentsModule\Http\Controller\DocumentsController@committee",
        "policies"              =>  "Finnito\DocumentsModule\Http\Controller\DocumentsController@policies",
        "minutes"               =>  "Finnito\DocumentsModule\Http\Controller\DocumentsController@minutes",
        "minutes/general"       =>  "Finnito\DocumentsModule\Http\Controller\DocumentsController@general",
    ];

    /**
     * The addon middleware.
     *
     * @type array|null
     */
    protected $middleware = [
        //Finnito\DocumentsModule\Http\Middleware\ExampleMiddleware::class
    ];

    /**
     * Addon group middleware.
     *
     * @var array
     */
    protected $groupMiddleware = [
        //'web' => [
        //    Finnito\DocumentsModule\Http\Middleware\ExampleMiddleware::class,
        //],
    ];

    /**
     * Addon route middleware.
     *
     * @type array|null
     */
    protected $routeMiddleware = [];

    /**
     * The addon event listeners.
     *
     * @type array|null
     */
    protected $listeners = [
        //Finnito\DocumentsModule\Event\ExampleEvent::class => [
        //    Finnito\DocumentsModule\Listener\ExampleListener::class,
        //],
    ];

    /**
     * The addon alias bindings.
     *
     * @type array|null
     */
    protected $aliases = [
        //'Example' => Finnito\DocumentsModule\Example::class
    ];

    /**
     * The addon class bindings.
     *
     * @type array|null
     */
    protected $bindings = [
        DocumentsPolicyCategoriesEntryModel::class => PolicyCategoryModel::class,
        DocumentsPoliciesEntryModel::class => PolicyModel::class,
        DocumentsGeneralMinuteCategoriesEntryModel::class => GeneralMinuteCategoryModel::class,
        DocumentsGeneralMinutesEntryModel::class => GeneralMinuteModel::class,
        DocumentsCommitteeDocumentCategoriesEntryModel::class => CommitteeDocumentCategoryModel::class,
        DocumentsCommitteeDocumentsEntryModel::class => CommitteeDocumentModel::class,
        DocumentsMinuteCategoriesEntryModel::class => MinuteCategoryModel::class,
        DocumentsMinutesEntryModel::class => MinuteModel::class,
        DocumentsDocumentCategoriesEntryModel::class => DocumentCategoryModel::class,
        DocumentsDocumentsEntryModel::class => DocumentModel::class,
    ];

    /**
     * The addon singleton bindings.
     *
     * @type array|null
     */
    protected $singletons = [
        PolicyCategoryRepositoryInterface::class => PolicyCategoryRepository::class,
        PolicyRepositoryInterface::class => PolicyRepository::class,
        GeneralMinuteCategoryRepositoryInterface::class => GeneralMinuteCategoryRepository::class,
        GeneralMinuteRepositoryInterface::class => GeneralMinuteRepository::class,
        CommitteeDocumentCategoryRepositoryInterface::class => CommitteeDocumentCategoryRepository::class,
        CommitteeDocumentRepositoryInterface::class => CommitteeDocumentRepository::class,
        MinuteCategoryRepositoryInterface::class => MinuteCategoryRepository::class,
        MinuteRepositoryInterface::class => MinuteRepository::class,
        DocumentCategoryRepositoryInterface::class => DocumentCategoryRepository::class,
        DocumentRepositoryInterface::class => DocumentRepository::class,
    ];

    /**
     * Additional service providers.
     *
     * @type array|null
     */
    protected $providers = [
        //\ExamplePackage\Provider\ExampleProvider::class
    ];

    /**
     * The addon view overrides.
     *
     * @type array|null
     */
    protected $overrides = [
        //'streams::errors/404' => 'module::errors/404',
        //'streams::errors/500' => 'module::errors/500',
    ];

    /**
     * The addon mobile-only view overrides.
     *
     * @type array|null
     */
    protected $mobile = [
        //'streams::errors/404' => 'module::mobile/errors/404',
        //'streams::errors/500' => 'module::mobile/errors/500',
    ];

    /**
     * Register the addon.
     */
    public function register()
    {
        // Run extra pre-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Boot the addon.
     */
    public function boot()
    {
        // Run extra post-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Map additional addon routes.
     *
     * @param Router $router
     */
    public function map(Router $router)
    {
        // Register dynamic routes here for example.
        // Use method injection or commands to bring in services.
    }

}
