<?php namespace Finnito\DocumentsModule\GeneralMinute\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface GeneralMinuteRepositoryInterface extends EntryRepositoryInterface
{

}
