<?php namespace Finnito\DocumentsModule\GeneralMinute;

use Finnito\DocumentsModule\GeneralMinute\Contract\GeneralMinuteInterface;
use Anomaly\Streams\Platform\Model\Documents\DocumentsGeneralMinutesEntryModel;

class GeneralMinuteModel extends DocumentsGeneralMinutesEntryModel implements GeneralMinuteInterface
{
    public function formats()
    {
        $formats = [];
        for ($i = 0; $i < sizeof($this->general_minutes); $i++) {
            $formats[] = "
            <a
                href='{$this->general_minutes[$i]->route("download")}'
                alt='{$this->general_minutes[$i]->name}'>
                .{$this->general_minutes[$i]->extension}
            </a>";
        }
        return implode("", $formats);
    }

    public function year()
    {
        return $this->date->format("Y");
    }
}
