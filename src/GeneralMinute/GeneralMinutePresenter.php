<?php namespace Finnito\DocumentsModule\GeneralMinute;

use Anomaly\Streams\Platform\Entry\EntryPresenter;

class GeneralMinutePresenter extends EntryPresenter
{
    public function formats()
    {
        $formats = [];
        for ($i = 0; $i < sizeof($this->general_minutes); $i++) {
            $formats[] = "
            <a
                href='{$this->general_minutes[$i]->route("download")}'
                alt='{$this->general_minutes[$i]->name}'>
                <span class='tag tag-info tag-sm'>.{$this->general_minutes[$i]->extension}</span>
            </a>";
        }
        return implode("", $formats);
    }
}
