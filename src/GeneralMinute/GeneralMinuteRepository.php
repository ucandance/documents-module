<?php namespace Finnito\DocumentsModule\GeneralMinute;

use Finnito\DocumentsModule\GeneralMinute\Contract\GeneralMinuteRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class GeneralMinuteRepository extends EntryRepository implements GeneralMinuteRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var GeneralMinuteModel
     */
    protected $model;

    /**
     * Create a new GeneralMinuteRepository instance.
     *
     * @param GeneralMinuteModel $model
     */
    public function __construct(GeneralMinuteModel $model)
    {
        $this->model = $model;
    }
}
