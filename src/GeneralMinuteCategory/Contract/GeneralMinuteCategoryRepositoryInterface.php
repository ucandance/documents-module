<?php namespace Finnito\DocumentsModule\GeneralMinuteCategory\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface GeneralMinuteCategoryRepositoryInterface extends EntryRepositoryInterface
{

}
