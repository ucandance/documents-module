<?php namespace Finnito\DocumentsModule\GeneralMinuteCategory;

use Finnito\DocumentsModule\GeneralMinuteCategory\Contract\GeneralMinuteCategoryInterface;
use Anomaly\Streams\Platform\Model\Documents\DocumentsGeneralMinuteCategoriesEntryModel;
use Finnito\DocumentsModule\GeneralMinute\GeneralMinuteModel;

class GeneralMinuteCategoryModel extends DocumentsGeneralMinuteCategoriesEntryModel implements GeneralMinuteCategoryInterface
{
    public function getEntries()
    {
        return $this
            ->hasMany(GeneralMinuteModel::class, "general_minute_category_id")
            ->get();
    }
}
