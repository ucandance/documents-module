<?php namespace Finnito\DocumentsModule\GeneralMinuteCategory;

use Finnito\DocumentsModule\GeneralMinuteCategory\Contract\GeneralMinuteCategoryRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class GeneralMinuteCategoryRepository extends EntryRepository implements GeneralMinuteCategoryRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var GeneralMinuteCategoryModel
     */
    protected $model;

    /**
     * Create a new GeneralMinuteCategoryRepository instance.
     *
     * @param GeneralMinuteCategoryModel $model
     */
    public function __construct(GeneralMinuteCategoryModel $model)
    {
        $this->model = $model;
    }
}
