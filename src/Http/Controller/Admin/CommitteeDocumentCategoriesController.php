<?php namespace Finnito\DocumentsModule\Http\Controller\Admin;

use Finnito\DocumentsModule\CommitteeDocumentCategory\Form\CommitteeDocumentCategoryFormBuilder;
use Finnito\DocumentsModule\CommitteeDocumentCategory\Table\CommitteeDocumentCategoryTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class CommitteeDocumentCategoriesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param CommitteeDocumentCategoryTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(CommitteeDocumentCategoryTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param CommitteeDocumentCategoryFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(CommitteeDocumentCategoryFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param CommitteeDocumentCategoryFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(CommitteeDocumentCategoryFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
