<?php namespace Finnito\DocumentsModule\Http\Controller\Admin;

use Finnito\DocumentsModule\CommitteeDocument\Form\CommitteeDocumentFormBuilder;
use Finnito\DocumentsModule\CommitteeDocument\Table\CommitteeDocumentTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class CommitteeDocumentsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param CommitteeDocumentTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(CommitteeDocumentTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param CommitteeDocumentFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(CommitteeDocumentFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param CommitteeDocumentFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(CommitteeDocumentFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
