<?php namespace Finnito\DocumentsModule\Http\Controller\Admin;

use Finnito\DocumentsModule\DocumentCategory\Form\DocumentCategoryFormBuilder;
use Finnito\DocumentsModule\DocumentCategory\Table\DocumentCategoryTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class DocumentCategoriesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param DocumentCategoryTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(DocumentCategoryTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param DocumentCategoryFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(DocumentCategoryFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param DocumentCategoryFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(DocumentCategoryFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
