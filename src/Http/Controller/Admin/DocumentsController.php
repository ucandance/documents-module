<?php namespace Finnito\DocumentsModule\Http\Controller\Admin;

use Finnito\DocumentsModule\Document\Form\DocumentFormBuilder;
use Finnito\DocumentsModule\Document\Table\DocumentTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class DocumentsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param DocumentTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(DocumentTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param DocumentFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(DocumentFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param DocumentFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(DocumentFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
