<?php namespace Finnito\DocumentsModule\Http\Controller\Admin;

use Finnito\DocumentsModule\GeneralMinuteCategory\Form\GeneralMinuteCategoryFormBuilder;
use Finnito\DocumentsModule\GeneralMinuteCategory\Table\GeneralMinuteCategoryTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class GeneralMinuteCategoriesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param GeneralMinuteCategoryTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(GeneralMinuteCategoryTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param GeneralMinuteCategoryFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(GeneralMinuteCategoryFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param GeneralMinuteCategoryFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(GeneralMinuteCategoryFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
