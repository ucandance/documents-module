<?php namespace Finnito\DocumentsModule\Http\Controller\Admin;

use Finnito\DocumentsModule\GeneralMinute\Form\GeneralMinuteFormBuilder;
use Finnito\DocumentsModule\GeneralMinute\Table\GeneralMinuteTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class GeneralMinutesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param GeneralMinuteTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(GeneralMinuteTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param GeneralMinuteFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(GeneralMinuteFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param GeneralMinuteFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(GeneralMinuteFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
