<?php namespace Finnito\DocumentsModule\Http\Controller\Admin;

use Finnito\DocumentsModule\MinuteCategory\Form\MinuteCategoryFormBuilder;
use Finnito\DocumentsModule\MinuteCategory\Table\MinuteCategoryTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class MinuteCategoriesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param MinuteCategoryTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(MinuteCategoryTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param MinuteCategoryFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(MinuteCategoryFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param MinuteCategoryFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(MinuteCategoryFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
