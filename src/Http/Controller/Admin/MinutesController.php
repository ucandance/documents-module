<?php namespace Finnito\DocumentsModule\Http\Controller\Admin;

use Finnito\DocumentsModule\Minute\Form\MinuteFormBuilder;
use Finnito\DocumentsModule\Minute\Table\MinuteTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class MinutesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param MinuteTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(MinuteTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param MinuteFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(MinuteFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param MinuteFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(MinuteFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
