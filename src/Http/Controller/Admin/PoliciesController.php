<?php namespace Finnito\DocumentsModule\Http\Controller\Admin;

use Finnito\DocumentsModule\Policy\Form\PolicyFormBuilder;
use Finnito\DocumentsModule\Policy\Table\PolicyTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class PoliciesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param PolicyTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PolicyTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param PolicyFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(PolicyFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param PolicyFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(PolicyFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
