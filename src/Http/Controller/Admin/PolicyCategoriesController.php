<?php namespace Finnito\DocumentsModule\Http\Controller\Admin;

use Finnito\DocumentsModule\PolicyCategory\Form\PolicyCategoryFormBuilder;
use Finnito\DocumentsModule\PolicyCategory\Table\PolicyCategoryTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class PolicyCategoriesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param PolicyCategoryTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PolicyCategoryTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param PolicyCategoryFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(PolicyCategoryFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param PolicyCategoryFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(PolicyCategoryFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
