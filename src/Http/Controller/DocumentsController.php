<?php namespace Finnito\DocumentsModule\Http\Controller;

use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Finnito\DocumentsModule\DocumentCategory\Contract\DocumentCategoryRepositoryInterface;
use Finnito\DocumentsModule\CommitteeDocumentCategory\Contract\CommitteeDocumentCategoryRepositoryInterface;
use Finnito\DocumentsModule\PolicyCategory\Contract\PolicyCategoryRepositoryInterface;
use Finnito\DocumentsModule\MinuteCategory\Contract\MinuteCategoryRepositoryInterface;
use Finnito\DocumentsModule\GeneralMinuteCategory\Contract\GeneralMinuteCategoryRepositoryInterface;
use Illuminate\Contracts\Auth\Guard;

/**
 * Class DocumentsController
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class DocumentsController extends PublicController
{

    /**
     * Return an index of all secure documents.
     *
     * @param DocumentCategoryRepositoryInterface $categories
     * @param Guard $auth
     * @return \Illuminate\Contracts\View\View|mixed
     */
    public function documents(DocumentCategoryRepositoryInterface $categories, Guard $auth)
    {
        if (!$user = $auth->user()) {
            redirect('/login?redirect='.url()->previous())->send();
        }

        if (!$user->hasAnyRole(["admin", "committee", "volunteer"])) {
            abort(403);
        }

        // Breadcrumbs
        $this->breadcrumbs->add("Documents", "/documents");

        // Meta Information
        $this->template->set("meta_title", "Documents");

        $attributes = ["name", "year", "current", "formats"];
        $filters = ["current", "archived"];
        return $this->view->make(
            "finnito.module.documents::secure/index",
            [
                "categories" => $categories->all(),
                "attributes" => $attributes,
                "filters" => $filters,
            ]
        );
    }

    /**
     * Return an index of all committee documents.
     *
     * @param CommitteeDocumentCategoryRepositoryInterface $categories
     * @param Guard $auth
     * @return \Illuminate\Contracts\View\View|mixed
     */
    public function committee(CommitteeDocumentCategoryRepositoryInterface $categories, Guard $auth)
    {
        if (!$user = $auth->user()) {
            redirect('/login?redirect='.url()->previous())->send();
        }

        if (!$user->hasAnyRole(["admin", "committee", "volunteer"])) {
            abort(403);
        }

        // Breadcrumbs
        $this->breadcrumbs->add("Documents", "/documents");
        $this->breadcrumbs->add("Committee Documents", $this->request->path());

        // Meta Information
        $this->template->set("meta_title", "Committee Documents");

        $attributes = ["name", "year", "formats"];
        $filters = [];
        return $this->view->make(
            "finnito.module.documents::secure/committee-documents",
            [
                "categories" => $categories->all(),
                "attributes" => $attributes,
                "filters" => $filters,
            ]
        );
    }

    /**
     * Return an index of all policies.
     *
     * @param PolicyCategoryRepositoryInterface $categories
     * @param Guard $auth
     * @return \Illuminate\Contracts\View\View|mixed
     */
    public function policies(PolicyCategoryRepositoryInterface $categories, Guard $auth)
    {
        if (!$user = $auth->user()) {
            redirect('/login?redirect='.url()->previous())->send();
        }

        if (!$user->hasAnyRole(["admin", "committee", "volunteer"])) {
            abort(403);
        }

        // Breadcrumbs
        $this->breadcrumbs->add("Policies", "/policies");

        // Meta Information
        $this->template->set("meta_title", "Policies");

        $attributes = ["name", "year", "formats"];
        $filters = [];
        return $this->view->make(
            "finnito.module.documents::secure/policies",
            [
                "categories" => $categories->all(),
                "attributes" => $attributes,
                "filters" => $filters,
            ]
        );
    }

    /**
     * Return an index of all committee minutes.
     *
     * @param MinuteCategoryRepositoryInterface $categories
     * @param Guard $auth
     * @return \Illuminate\Contracts\View\View|mixed
     */
    public function minutes(MinuteCategoryRepositoryInterface $categories, Guard $auth)
    {
        if (!$user = $auth->user()) {
            redirect('/login?redirect='.url()->previous())->send();
        }

        if (!$user->hasAnyRole(["admin", "committee", "volunteer"])) {
            abort(403);
        }

        // Breadcrumbs
        $this->breadcrumbs->add("Minutes", "/minutes");

        // Meta Information
        $this->template->set("meta_title", "Minutes");

        $attributes = ["name", "year", "formats"];
        $filters = [];
        return $this->view->make(
            "finnito.module.documents::secure/minutes",
            [
                "categories" => $categories->newQuery()->orderBy("name", "DESC")->get(),
                "attributes" => $attributes,
                "filters" => $filters,
            ]
        );
    }

    /**
     * Return an index of general minutes.
     *
     * @param GeneralMinuteCategoryRepositoryInterface $categories
     * @param Guard $auth
     * @return \Illuminate\Contracts\View\View|mixed
     */
    public function general(GeneralMinuteCategoryRepositoryInterface $categories, Guard $auth)
    {
        if (!$user = $auth->user()) {
            redirect('/login?redirect='.url()->previous())->send();
        }

        if (!$user->hasAnyRole(["admin", "committee", "volunteer"])) {
            abort(403);
        }

        // Breadcrumbs
        $this->breadcrumbs->add("Minutes", "/minutes");
        $this->breadcrumbs->add("General", $this->request->path());

        // Meta Information
        $this->template->set("meta_title", "General Minutes");

        $attributes = ["name", "year", "formats"];
        $filters = [];
        return $this->view->make(
            "finnito.module.documents::secure/general",
            [
                "categories" => $categories->all(),
                "attributes" => $attributes,
                "filters" => $filters,
            ]
        );
    }
}
