<?php namespace Finnito\DocumentsModule\Minute\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface MinuteRepositoryInterface extends EntryRepositoryInterface
{

}
