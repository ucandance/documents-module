<?php namespace Finnito\DocumentsModule\Minute;

use Finnito\DocumentsModule\Minute\Contract\MinuteInterface;
use Anomaly\Streams\Platform\Model\Documents\DocumentsMinutesEntryModel;

class MinuteModel extends DocumentsMinutesEntryModel implements MinuteInterface
{
    public function formats()
    {
        $formats = [];
        for ($i = 0; $i < sizeof($this->minutes); $i++) {
            $formats[] = "
            <a
                href='{$this->minutes[$i]->route("download")}'
                alt='{$this->minutes[$i]->name}'>
                .{$this->minutes[$i]->extension}
            </a>";
        }
        return implode("", $formats);
    }

    public function year()
    {
        return $this->date->format("Y");
    }
}
