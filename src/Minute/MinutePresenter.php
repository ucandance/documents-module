<?php namespace Finnito\DocumentsModule\Minute;

use Anomaly\Streams\Platform\Entry\EntryPresenter;

class MinutePresenter extends EntryPresenter
{
    public function formats()
    {
        $formats = [];
        for ($i = 0; $i < sizeof($this->minutes); $i++) {
            $formats[] = "
            <a
                href='{$this->minutes[$i]->route("download")}'
                alt='{$this->minutes[$i]->name}'>
                <span class='tag tag-info tag-sm'>.{$this->minutes[$i]->extension}</span>
            </a>";
        }
        return implode("", $formats);
    }
}
