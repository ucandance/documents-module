<?php namespace Finnito\DocumentsModule\Minute;

use Finnito\DocumentsModule\Minute\Contract\MinuteRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class MinuteRepository extends EntryRepository implements MinuteRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var MinuteModel
     */
    protected $model;

    /**
     * Create a new MinuteRepository instance.
     *
     * @param MinuteModel $model
     */
    public function __construct(MinuteModel $model)
    {
        $this->model = $model;
    }
}
