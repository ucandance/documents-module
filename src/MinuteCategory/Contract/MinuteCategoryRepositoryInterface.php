<?php namespace Finnito\DocumentsModule\MinuteCategory\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface MinuteCategoryRepositoryInterface extends EntryRepositoryInterface
{

}
