<?php namespace Finnito\DocumentsModule\MinuteCategory;

use Finnito\DocumentsModule\MinuteCategory\Contract\MinuteCategoryInterface;
use Anomaly\Streams\Platform\Model\Documents\DocumentsMinuteCategoriesEntryModel;
use Finnito\DocumentsModule\Minute\MinuteModel;

class MinuteCategoryModel extends DocumentsMinuteCategoriesEntryModel implements MinuteCategoryInterface
{
    public function getEntries()
    {
        return $this
            ->hasMany(MinuteModel::class, "minute_category_id")
            ->get();
    }
}
