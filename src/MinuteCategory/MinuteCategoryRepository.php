<?php namespace Finnito\DocumentsModule\MinuteCategory;

use Finnito\DocumentsModule\MinuteCategory\Contract\MinuteCategoryRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class MinuteCategoryRepository extends EntryRepository implements MinuteCategoryRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var MinuteCategoryModel
     */
    protected $model;

    /**
     * Create a new MinuteCategoryRepository instance.
     *
     * @param MinuteCategoryModel $model
     */
    public function __construct(MinuteCategoryModel $model)
    {
        $this->model = $model;
    }
}
