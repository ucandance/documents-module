<?php namespace Finnito\DocumentsModule\Policy\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface PolicyRepositoryInterface extends EntryRepositoryInterface
{

}
