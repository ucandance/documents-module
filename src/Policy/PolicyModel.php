<?php namespace Finnito\DocumentsModule\Policy;

use Finnito\DocumentsModule\Policy\Contract\PolicyInterface;
use Anomaly\Streams\Platform\Model\Documents\DocumentsPoliciesEntryModel;

class PolicyModel extends DocumentsPoliciesEntryModel implements PolicyInterface
{
    public function formats()
    {
        $formats = [];
        for ($i = 0; $i < sizeof($this->policy); $i++) {
            $formats[] = "
            <a
                href='{$this->policy[$i]->route("download")}'
                alt='{$this->policy[$i]->name}'>
                .{$this->policy[$i]->extension}
            </a>";
        }
        return implode("", $formats);
    }

    public function year()
    {
        return $this->date->format("Y");
    }
}
