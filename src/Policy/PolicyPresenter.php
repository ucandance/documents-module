<?php namespace Finnito\DocumentsModule\Policy;

use Anomaly\Streams\Platform\Entry\EntryPresenter;

class PolicyPresenter extends EntryPresenter
{
    public function formats()
    {
        $formats = [];
        for ($i = 0; $i < sizeof($this->policy); $i++) {
            $formats[] = "
            <a
                href='{$this->policy[$i]->route("download")}'
                alt='{$this->policy[$i]->name}'>
                <span class='tag tag-info tag-sm'>.{$this->policy[$i]->extension}</span>
            </a>";
        }
        return implode("", $formats);
    }
}
