<?php namespace Finnito\DocumentsModule\Policy;

use Finnito\DocumentsModule\Policy\Contract\PolicyRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class PolicyRepository extends EntryRepository implements PolicyRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var PolicyModel
     */
    protected $model;

    /**
     * Create a new PolicyRepository instance.
     *
     * @param PolicyModel $model
     */
    public function __construct(PolicyModel $model)
    {
        $this->model = $model;
    }
}
