<?php namespace Finnito\DocumentsModule\Policy\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

class PolicyTableBuilder extends TableBuilder
{

    /**
     * The table views.
     *
     * @var array|string
     */
    protected $views = [];

    /**
     * The table filters.
     *
     * @var array|string
     */
    protected $filters = [];

    /**
     * The table columns.
     *
     * @var array|string
     */
    protected $columns = [
        "name",
        "policy_category",
        "date" => [
            "heading" => "Date",
            "value" => "entry.date.format('d-m-Y')",
        ],
        "downloads" => [
            "heading" => "Formats",
            "value" => "entry.formats()",
        ],
    ];

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'edit'
    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete'
    ];

    /**
     * The table options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The table assets.
     *
     * @var array
     */
    protected $assets = [];

}
