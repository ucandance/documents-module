<?php namespace Finnito\DocumentsModule\PolicyCategory\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface PolicyCategoryRepositoryInterface extends EntryRepositoryInterface
{

}
