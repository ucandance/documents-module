<?php namespace Finnito\DocumentsModule\PolicyCategory;

use Finnito\DocumentsModule\PolicyCategory\Contract\PolicyCategoryInterface;
use Anomaly\Streams\Platform\Model\Documents\DocumentsPolicyCategoriesEntryModel;
use Finnito\DocumentsModule\Policy\PolicyModel;

class PolicyCategoryModel extends DocumentsPolicyCategoriesEntryModel implements PolicyCategoryInterface
{
    public function getEntries()
    {
        return $this
            ->hasMany(PolicyModel::class, "policy_category_id")
            ->get();
    }
}
