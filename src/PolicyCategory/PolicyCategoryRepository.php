<?php namespace Finnito\DocumentsModule\PolicyCategory;

use Finnito\DocumentsModule\PolicyCategory\Contract\PolicyCategoryRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class PolicyCategoryRepository extends EntryRepository implements PolicyCategoryRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var PolicyCategoryModel
     */
    protected $model;

    /**
     * Create a new PolicyCategoryRepository instance.
     *
     * @param PolicyCategoryModel $model
     */
    public function __construct(PolicyCategoryModel $model)
    {
        $this->model = $model;
    }
}
